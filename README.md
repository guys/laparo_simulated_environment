# Qualitative Comparison of Image Stitching Algorithms for Multi-Cameras Systems in Laparoscopy

<div align="center">
<img src="database/laparo/conf_free_3/Camera.Pi_left.png" width="425"" /> <img src="database/laparo/conf_free_3/Camera.Pi_right.png" width="425"/> 
</div>
<div align="center">
<img src="demo.mp4" width="825" />
</div>


Database and simulated environment from the paper:
**"Qualitative Comparison of Image Stitching Algorithms for Multi-Cameras Systems in Laparoscopy" by Sylvain Guy, Jean-Loup Haberbusch, Emmanuel Promayon, Stéphane Mancini and Sandrine Voros.** 
Please cite this paper if you use this environment for your work.


## Simulated environment: a simulated environment to test multicameras prototype in laparoscopy

A blender (https://www.blender.org/) environment "simulated_environment_free_final.blend" contains a laparoscopic scene with organs, forceps, endoscope and a multicameras prototype (GVS). 

Some elements/textures of the Blender scene were taken from other people but are free to use. Details about licence and authors of these components are listed in the "models" and "textures" folders.

#### Install blender

This has been tested with Blender 2.83.1. This can be installed in the home with:
``` bash
cd ~
wget https://download.blender.org/release/Blender2.83/blender-2.83.1-linux64.tar.xz 
tar xf blender-2.83.1-linux64.tar.xz
cd blender-2.83.1-linux64/
echo "export PATH=$PATH:~/blender-2.83.1-linux64" >> ~/.bashrc

source ~/.bashrc
```






#### Render interactively with the addon

This environment goes with a blender addon, a python module called moduleGVS.py. This module enables fast modification of the scene parameters. To run the blender environment with the module, just execute in a terminal:
    
    
    blender simulated_environment_free_final.blend -P moduleGVS.py


​    
You'll get the scene with an addon that is structured as follow:
​    
- Left forceps:
    set the absolute world position and orientation of the left forceps

- Right forceps:
    set the absolute world position and orientation of the right forceps

- Endoscope:
    - set the depth of the endoscope inside the abdominal cavity
    - camera:
        - intrinsic camera parameters: (1)
        - light power
        - a potential halo (useful when you want to have some light without the endoscope out)
    
- Prototype GVS:
    - absolute position/orientation of the multicameras prototype (2)
    - cameras configurations, each camera can be set differently. There are 3 cameras configurations available (2 configurations with misumi cameras and one with a raspberry pi cameras). Each of these cameras can be modified using:
        - intrinsic camera parameters: (1)
        - light power
        - orientation: you can increment the 3 euler angles of the camera, the transformation will be relative to the camera axis.
        - interdistance cameras: this parameter is used to adapt the distance between the two cameras of the prototype. Warning: it's likely that you need to adjust symetrically the orientations and distances between the two cameras you want to used, so you need to select each camera and modify consecutively.

- Rendering:
    - engine: cycles is much more accurate but much slower than Eevee
    - samples: increase to improve rendering quality
    ...
    - resolution: output resolutions. You can also select a preset of resolution corresponding to the available cameras
    
    THEN select the cameras you want to render from. Typically it should be a right and a left camera from one of the three available confs (misumi, ...)
    - path where to save the results
    
    FINALLY click on "Start" to run the rendering and you should get the results in the selected output folder.



(1) intrinsic parameters are always the same parameters for each camera 
(endoscope, multicam prototype), like focal, aperture, clipping ...

(2) Warning: these absolute position/orientation of the multicameras prototype
are currently not really useful because the positions of the cameras of the 
prototype are set relative to the world coordinates and not relative to the 
prototype coordinates.


#### Render in command line with a configuration file

Another possibility to use the addon is in a non-interactive way using configurations files. An example of configuration file is 
example/config_scene.json. For most of its parameters it follows the previously explained addon so I won't get in these details and just point out the differences/subtilities.

- The render engine can only be: "CYCLES" or "BLENDER_EEVEE"
- "render_everything" should always be true cause otherwise you won't get any rendering.
- "initial_conf_cams" tells from which cameras configurations you want to start before any other modifications specified in the json file. It must be in ["misumi", "misumi_norot", "pi"]
- "proto_gvs"->"position" and ""proto_gvs"->"orientation" should have no impact on the rendering cause camera positions and orientations are not linked to the prototype coordinates (this is a choice that could be matter of discussion...). The only impact it can have if its the prototype goes in the cameras field's of
view.
-"proto_gvs"->"camera_left"("camera_right")->"orientation"("translation") are geometric transformations relative to the initial camera positions and axis (specified in "initial_conf_cams")

Example: in example/config_scene.json

We have two forceps and an endoscope that has its default position (depth). This endoscope emits a few light.
The multicameras prototype has misumi cameras initialized with their default configuration (a slight rotation and an interdistance of a few centimeters, cf inside the blender 3D envi.). On top of that we add a slight rotation 0.05 for each of the two cameras so that they look a bit more away from each other. We also add a slight translation of 0.01 between these two such that to test
with an increased parallax problem.

To run this configuration and get the rendered results, just run this in a 
terminal:


    blender simulated_environment_free_final.blend -b -P moduleGVS.py -- -i example/eeve/config_scene_faster.json


## Database

Some examples of rendered images and videos can be found in the "/database" folder

## License
TODO


## Citation
```
@article{guy2020stitching,
  title={Qualitative Comparison of Image Stitching Algorithms for Multi-Cameras Systems in Laparoscopy" by Sylvain Guy, Jean-Loup Haberbusch, Emmanuel Promayon, Stéphane Mancini and Sandrine Voros},
  author={Guy, Sylvain and Haberbusch, Jean-Loup and Promayon, Emmanuel and Mancini, Stéphane and Voros, Sandrine},
  journal={},
  year={2021}
}
```
