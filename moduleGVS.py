import bpy
import os, sys
import json
import argparse
import math


def add_local_files():
    dir = os.path.dirname(bpy.data.filepath)
    if not dir in sys.path:
        sys.path.append(dir)


add_local_files()
from tools import *


bl_info = {
    "name": "GVS Addon",
    "author": "Sylvain Guy",
    "version": (1, 0),
    "blender": (2.83, 1),
    "location": "View3d > Tool",
    "warnings": "",
    "wiki_url": "",
    "category": "GVS"
    }

# Import elements that we need to manipulate
import config_forceps
import config_endoscope
import config_proto_gvs
import config_renderer
import config_fisheye

dict_properties = {
    **config_endoscope.properties_endoscope,
    **config_forceps.properties_left_forceps,
    **config_forceps.properties_right_forceps,
    **config_proto_gvs.properties_gvs_prototype,
    **config_renderer.properties_rendering,
    **config_fisheye.properties_fisheye,
    "config_path": bpy.props.StringProperty(name="config_path",
                                            default="/tmp/",
                                            subtype="FILE_PATH")
}
CustomPropertyGroup = create_custom_property_group(dict_properties)

def register():
    register_list_elements([CustomPropertyGroup])
    bpy.types.Scene.custom_props = bpy.props.PointerProperty(type=CustomPropertyGroup)
    register_list_elements(
                           config_endoscope.to_be_registered +
                           config_forceps.to_be_registered +
                           config_proto_gvs.to_be_registered +
                           config_fisheye.to_be_registered +
                           [StringValue, StringCollection] +
                           config_renderer.to_be_registered+
                            to_be_registered)


def unregister():
    try:
        del bpy.types.Scene.custom_props 
    except Exception as e:
        print("Exception custom prop: ", e)
        pass

    unregister_list_elements([CustomPropertyGroup]+config_endoscope.to_be_registered +
                             config_forceps.to_be_registered +
                             config_proto_gvs.to_be_registered +
                             config_fisheye.to_be_registered +
                             config_renderer.to_be_registered+
                             to_be_registered)


class Render_From_Config_File(bpy.types.Panel):
    """ Multi-cameras prototype (GVS) config as a whole """

    bl_label = "Render_from_config"
    bl_idname = "RENDERCONFIG_PT"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_category = 'GVS'
    bl_options = {'DEFAULT_CLOSED'}

    

    #def invoke(self, context, event):
    #    context.window_manager.fileselect_add(self)
    #    return {'RUNNING_MODAL'}

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        layout.prop(context.scene.custom_props, "config_path", text="Path to the config file")
        row.label(text="Apply config", icon="SCENE")
        row.operator('gvs.apply_config_without_rendering', text='Start')

        row.label(text="Render config", icon="SCENE")
        row.operator('gvs.render_config', text='Start')


class GVS_APPLYCONFIG_OPERATOR(bpy.types.Operator):
    """ Render images from selected cameras """

    bl_label = "Apply config"
    bl_idname = 'gvs.apply_config_without_rendering'

    def execute(self, context):
        apply_from_config(context.scene.custom_props.config_path)
        return {'FINISHED'}


class GVS_RENDERCONFIG_OPERATOR(bpy.types.Operator):
    """ Render images from selected cameras """

    bl_label = "Render config"
    bl_idname = 'gvs.render_config'

    def execute(self, context):
        render_from_config(context.scene.custom_props.config_path)
        return {'FINISHED'}



to_be_registered = [Render_From_Config_File, GVS_APPLYCONFIG_OPERATOR, GVS_RENDERCONFIG_OPERATOR]


def apply_from_config(config_file):
    modal = False
  
    # Lets start by reading the json configuration file
    
    with open(config_file) as f:
        d = json.load(f)
    
    # Now lets apply all settings and operations 
    # Place the forceps left at the correct position/orientation
    forceps_left_position = d["forceps_left"]["position"]
    forceps_left_orientation = d["forceps_left"]["orientation"]
    bpy.context.scene.custom_props.position_forceps_left_slider = forceps_left_position
    bpy.context.scene.custom_props.orientation_forceps_left_slider = forceps_left_orientation
    bpy.ops.transform.position_forceps_left_operator()
    bpy.ops.transform.orientation_forceps_left_operator()
    
    # Place the forceps right at the correct position/orientation
    forceps_right_position = d["forceps_right"]["position"]
    forceps_right_orientation = d["forceps_right"]["orientation"]
    bpy.context.scene.custom_props.position_forceps_right_slider = forceps_right_position
    bpy.context.scene.custom_props.orientation_forceps_right_slider = forceps_right_orientation
    bpy.ops.transform.position_forceps_right_operator()
    bpy.ops.transform.orientation_forceps_right_operator()
    
    # Translate endoscope
    translation_endoscope = d["endoscope"]["depth"]
    bpy.context.scene.custom_props.depth_endoscope_slider = translation_endoscope
    bpy.ops.transform.depth_operator()
    
    # Set lighting endoscope camera
    light_endoscope_camera = d["endoscope"]["camera"]["lighting"]
    led_light_0 = bpy.data.objects['Endoscope_spot_0'].data 
    led_light_1 = bpy.data.objects['Endoscope_spot_1'].data
    led_light_2 = bpy.data.objects['Endoscope_spot_2'].data
    led_light_3 = bpy.data.objects['Endoscope_spot_3'].data
    led_light_4 = bpy.data.objects['Endoscope_spot_4'].data
    led_light_5 = bpy.data.objects['Endoscope_spot_5'].data
    led_light_6 = bpy.data.objects['Endoscope_spot_6'].data
    led_light_0.energy = light_endoscope_camera
    led_light_1.energy = light_endoscope_camera
    led_light_2.energy = light_endoscope_camera
    led_light_3.energy = light_endoscope_camera
    led_light_4.energy = light_endoscope_camera
    led_light_5.energy = light_endoscope_camera
    led_light_6.energy = light_endoscope_camera
    
    # Set intrinsic parameters of endoscope camera
    intrinsic_endoscope_camera = d["endoscope"]["camera"]["intrinsic"]
    camera = bpy.data.objects['Endoscope_camera'].data
    camera.lens_unit = "FOV"
    camera.lens = intrinsic_endoscope_camera["focal_length"]
    camera.angle_x = intrinsic_endoscope_camera["horizontal_fov"]
    camera.angle_y = intrinsic_endoscope_camera["vertical_fov"]
    camera.clip_start = intrinsic_endoscope_camera["clip_start"]
    camera.sensor_fit = "AUTO"
    camera.sensor_height = intrinsic_endoscope_camera["sensor_height"]
    camera.sensor_width = intrinsic_endoscope_camera["sensor_width"]
    camera.dof.focus_distance = 10
    camera.dof.aperture_fstop = intrinsic_endoscope_camera["f_stop"]



    # Set the right camera prototype
    if "proto_gvs" in d.keys():
        # Set the GVS prototype
        # position
        gvs_proto_position = d["proto_gvs"]["position"]
        bpy.context.scene.custom_props.position_proto_slider = gvs_proto_position
        bpy.ops.transform.position_proto_operator()

        # orientation
        gvs_proto_orientation = d["proto_gvs"]["orientation"]
        bpy.context.scene.custom_props.orientation_proto_slider = gvs_proto_orientation
        bpy.ops.transform.orientation_proto_operator()

        # Set the cameras of the prototype
        # Start from existing preset
        bpy.context.scene.custom_props.gvs_camera_left_misumi = False
        bpy.context.scene.custom_props.gvs_camera_right_misumi = False
        bpy.context.scene.custom_props.gvs_camera_left_misuminorot = False
        bpy.context.scene.custom_props.gvs_camera_right_misuminorot = False
        bpy.context.scene.custom_props.gvs_camera_left_pi = False
        bpy.context.scene.custom_props.gvs_camera_right_pi = False
        if d["proto_gvs"]["initial_conf_cams"] == "misumi":
            camera_left = bpy.data.objects['Camera.Misumi_left']
            camera_right = bpy.data.objects['Camera.Misumi_right']
            bpy.context.scene.custom_props.gvs_camera_left_misumi = True
            bpy.context.scene.custom_props.gvs_camera_right_misumi = True
        elif d["proto_gvs"]["initial_conf_cams"] == "misumi_norot":
            camera_left = bpy.data.objects['Camera.Misumi_left_norot']
            camera_right = bpy.data.objects['Camera.Misumi_right_norot']
            bpy.context.scene.custom_props.gvs_camera_left_misuminorot = True
            bpy.context.scene.custom_props.gvs_camera_right_misuminorot = True
        elif d["proto_gvs"]["initial_conf_cams"] == "pi":
            camera_left = bpy.data.objects['Camera.Pi_left']
            camera_right = bpy.data.objects['Camera.Pi_right']
            bpy.context.scene.custom_props.gvs_camera_left_pi = True
            bpy.context.scene.custom_props.gvs_camera_right_pi = True
        else:
            raise Exception

        # left camera
        intrinsic_gvs_camera_left = d["proto_gvs"]["camera_left"]["intrinsic"]
        camera_left.data.lens_unit = "FOV"
        camera_left.data.lens = intrinsic_gvs_camera_left["focal_length"]
        camera_left.data.angle_x = intrinsic_gvs_camera_left["horizontal_fov"]
        camera_left.data.angle_y = intrinsic_gvs_camera_left["vertical_fov"]
        camera_left.data.clip_start = intrinsic_gvs_camera_left["clip_start"]
        camera_left.data.sensor_fit = "AUTO"
        camera_left.data.sensor_height = intrinsic_gvs_camera_left["sensor_height"]
        camera_left.data.sensor_width = intrinsic_gvs_camera_left["sensor_width"]
        camera_left.data.lens = intrinsic_gvs_camera_left["focal_length"]
        camera_left.data.angle_x = intrinsic_gvs_camera_left["horizontal_fov"]
        camera_left.data.angle_y = intrinsic_gvs_camera_left["vertical_fov"]
        camera_left.data.dof.focus_distance = 10
        camera_left.data.dof.aperture_fstop = intrinsic_gvs_camera_left["f_stop"]

        light_gvs_camera_left = d["proto_gvs"]["camera_left"]["lighting"]
        led_light_1 = bpy.data.objects['GVS_cam_1_spot_1'].data
        led_light_2 = bpy.data.objects['GVS_cam_1_spot_2'].data
        led_light_3 = bpy.data.objects['GVS_cam_1_spot_3'].data
        led_light_4 = bpy.data.objects['GVS_cam_1_spot_4'].data
        led_light_5 = bpy.data.objects['GVS_cam_1_spot_5'].data
        led_light_6 = bpy.data.objects['GVS_cam_1_spot_6'].data
        led_light_0.energy = light_gvs_camera_left
        led_light_1.energy = light_gvs_camera_left
        led_light_2.energy = light_gvs_camera_left
        led_light_3.energy = light_gvs_camera_left
        led_light_4.energy = light_gvs_camera_left
        led_light_5.energy = light_gvs_camera_left
        led_light_6.energy = light_gvs_camera_left

        # right camera
        intrinsic_gvs_camera_right = d["proto_gvs"]["camera_right"]["intrinsic"]
        camera_right.data.lens_unit = "FOV"
        camera_right.data.lens = intrinsic_gvs_camera_right["focal_length"]
        camera_right.data.angle_x = intrinsic_gvs_camera_right["horizontal_fov"]
        camera_right.data.angle_y = intrinsic_gvs_camera_right["vertical_fov"]
        camera_right.data.clip_start = intrinsic_gvs_camera_right["clip_start"]

        camera_right.data.sensor_height = intrinsic_gvs_camera_right["sensor_height"]
        camera_right.data.sensor_width = intrinsic_gvs_camera_right["sensor_width"]
        camera_right.data.lens = intrinsic_gvs_camera_right["focal_length"]
        camera_right.data.angle_x = intrinsic_gvs_camera_right["horizontal_fov"]
        camera_right.data.angle_y = intrinsic_gvs_camera_right["vertical_fov"]
        camera_right.data.sensor_fit = "AUTO"
        camera_right.data.dof.focus_distance = 10
        camera_right.data.dof.aperture_fstop = intrinsic_gvs_camera_right["f_stop"]

        light_gvs_camera_right = d["proto_gvs"]["camera_right"]["lighting"]
        led_light_1 = bpy.data.objects['GVS_cam_2_spot_1'].data
        led_light_2 = bpy.data.objects['GVS_cam_2_spot_2'].data
        led_light_3 = bpy.data.objects['GVS_cam_2_spot_3'].data
        led_light_4 = bpy.data.objects['GVS_cam_2_spot_4'].data
        led_light_5 = bpy.data.objects['GVS_cam_2_spot_5'].data
        led_light_6 = bpy.data.objects['GVS_cam_2_spot_6'].data
        led_light_0.energy = light_gvs_camera_right
        led_light_1.energy = light_gvs_camera_right
        led_light_2.energy = light_gvs_camera_right
        led_light_3.energy = light_gvs_camera_right
        led_light_4.energy = light_gvs_camera_right
        led_light_5.energy = light_gvs_camera_right
        led_light_6.energy = light_gvs_camera_right

        # Apply rotation and intra-distance shift to the cameras
        bpy.context.scene.custom_props.camera_to_modify = camera_left.name
        bpy.context.scene.custom_props.orientation_cams_proto = d["proto_gvs"]["camera_left"]["orientation"]
        bpy.ops.transform.orientation_camselected_proto_operator()
        bpy.context.scene.custom_props.distance_intercams_proto_slider = d["proto_gvs"]["camera_left"]["translation"]
        bpy.ops.transform.interdistance_cams_proto_operator()

        bpy.context.scene.custom_props.camera_to_modify = camera_right.name
        bpy.context.scene.custom_props.orientation_cams_proto = d["proto_gvs"]["camera_right"]["orientation"]
        bpy.ops.transform.orientation_camselected_proto_operator()
        bpy.context.scene.custom_props.distance_intercams_proto_slider = d["proto_gvs"]["camera_right"]["translation"]
        bpy.ops.transform.interdistance_cams_proto_operator()

        # Select the cameras
        bpy.context.scene.custom_props[camera_left.name] = 1
        bpy.context.scene.custom_props[camera_right.name] = 1


    elif "proto_fisheye" in d.keys():
        # Set the fisheye prototype

        # The position and orientation of the endoscope is the same as GVS prototype
        # position
        gvs_proto_position = d["proto_fisheye"]["position"]
        bpy.context.scene.custom_props.position_proto_slider = gvs_proto_position
        bpy.ops.transform.position_proto_operator()

        # orientation
        gvs_proto_orientation = d["proto_fisheye"]["orientation"]
        bpy.context.scene.custom_props.orientation_proto_slider = gvs_proto_orientation
        bpy.ops.transform.orientation_proto_operator()

        # lights
        light_gvs_camera_right = d["proto_fisheye"]["virtual_light"] #0.5 #d["proto_gvs"]["camera_right"]["lighting"]
        led_light_1 = bpy.data.objects['GVS_cam_2_spot_1'].data
        led_light_2 = bpy.data.objects['GVS_cam_2_spot_2'].data
        led_light_3 = bpy.data.objects['GVS_cam_2_spot_3'].data
        led_light_4 = bpy.data.objects['GVS_cam_2_spot_4'].data
        led_light_5 = bpy.data.objects['GVS_cam_2_spot_5'].data
        led_light_6 = bpy.data.objects['GVS_cam_2_spot_6'].data
        led_light_0.energy = light_gvs_camera_right
        led_light_1.energy = light_gvs_camera_right
        led_light_2.energy = light_gvs_camera_right
        led_light_3.energy = light_gvs_camera_right
        led_light_4.energy = light_gvs_camera_right
        led_light_5.energy = light_gvs_camera_right
        led_light_6.energy = light_gvs_camera_right

        light_gvs_camera_left = d["proto_fisheye"]["virtual_light"] #d["proto_gvs"]["camera_left"]["lighting"]
        led_light_1 = bpy.data.objects['GVS_cam_1_spot_1'].data
        led_light_2 = bpy.data.objects['GVS_cam_1_spot_2'].data
        led_light_3 = bpy.data.objects['GVS_cam_1_spot_3'].data
        led_light_4 = bpy.data.objects['GVS_cam_1_spot_4'].data
        led_light_5 = bpy.data.objects['GVS_cam_1_spot_5'].data
        led_light_6 = bpy.data.objects['GVS_cam_1_spot_6'].data
        led_light_0.energy = light_gvs_camera_left
        led_light_1.energy = light_gvs_camera_left
        led_light_2.energy = light_gvs_camera_left
        led_light_3.energy = light_gvs_camera_left
        led_light_4.energy = light_gvs_camera_left
        led_light_5.energy = light_gvs_camera_left
        led_light_6.energy = light_gvs_camera_left

        # Set the cameras of the prototype
        ## Start from existing preset
        #if d["proto_gvs"]["initial_conf_cams"] == "misumi":
        #    camera_left = bpy.data.objects['Camera.Misumi_left']
        #    camera_right = bpy.data.objects['Camera.Misumi_right']
        #    bpy.context.scene.custom_props.gvs_camera_left_misumi = 1
        #    bpy.context.scene.custom_props.gvs_camera_right_misumi = 1

        # left camera
        # nothing to parametrize
        camera = bpy.data.objects["Camera.Fisheye_left"].data
        camera.cycles.fisheye_fov = math.radians(d["proto_fisheye"]["camera_left"]["fov"])

        # right camera
        # nothing to parametrize
        camera = bpy.data.objects["Camera.Fisheye_right"].data
        camera.cycles.fisheye_fov = math.radians(d["proto_fisheye"]["camera_right"]["fov"])

        # light
        # TODO

        # adjust left camera
        bpy.context.scene.custom_props.camera_to_modify_fisheye = "Camera.Fisheye_left"
        bpy.context.scene.custom_props.rotation_fisheye_slider2 = d["proto_fisheye"]["camera_left"]["orientation"]
        bpy.context.scene.custom_props.translate_fisheye_slider = d["proto_fisheye"]["camera_left"]["translation"]

        bpy.ops.transform.orientation_fisheye()
        bpy.ops.transform.translate_fisheye_operator()

        # Adjust right camera
        bpy.context.scene.custom_props.camera_to_modify_fisheye = "Camera.Fisheye_right"
        bpy.context.scene.custom_props.rotation_fisheye_slider2 = d["proto_fisheye"]["camera_right"]["orientation"]
        bpy.context.scene.custom_props.translate_fisheye_slider = d["proto_fisheye"]["camera_right"]["translation"]

        bpy.ops.transform.orientation_fisheye()
        bpy.ops.transform.translate_fisheye_operator()

        # Select the cameras
        bpy.context.scene.custom_props["Camera.Fisheye_right"] = 1
        bpy.context.scene.custom_props["Camera.Fisheye_left"] = 1
    else:
        print("Issue no prototype found")
        assert(False)



def render_from_config(config_file):
    with open(config_file) as f:
        d = json.load(f)
    # Rendering
    bpy.context.scene.render.engine = d["render"]["engine"]
    if d["render"]["engine"] == "BLENDER_EEVEE":
        bpy.context.scene.eevee.taa_render_samples = d["render"]["samples"]
    else:
        bpy.context.scene.cycles.samples = d["render"]["samples"]
    bpy.data.scenes["Scene"].view_settings.exposure = d["render"]["exposure"]
    bpy.data.scenes["Scene"].render.resolution_x = d["render"]["resolution_x"]
    bpy.data.scenes["Scene"].render.resolution_y = d["render"]["resolution_y"]
    bpy.context.scene.custom_props.output_path = d["render"]["output_path"]

    if d["render"]["bake_lighting"]:
        bpy.ops.render.bake_lighting()
    if d["render"]["render_everything"]:
        # Set the device_type
        bpy.context.preferences.addons[
            "cycles"
        ].preferences.compute_device_type = "CUDA"  # or "OPENCL"

        # Set the device and feature set
        bpy.context.scene.cycles.device = "GPU"

        # get_devices() to let Blender detects GPU device
        bpy.context.preferences.addons["cycles"].preferences.get_devices()
        for d in bpy.context.preferences.addons["cycles"].preferences.devices:
            d["use"] = 1  # Using all devices, include GPU and CPU
        bpy.ops.render.my_render()


if __name__ == '__main__':
    
    """ Example of a run: 
       
        blender simulated_environment_free_final.blend -b -P moduleGVS.py -- -i example/eeve/config_scene_faster.json
        
    """

    #  List here all elements available in the Blender scene
    l = ["left_forceps", "right_forceps", "endoscope", "gvs"]

    unregister()

    # Register everything
    try:
        register()    
    except Exception as e:
        print("Already registered")

    # print(" Init custom prop ", bpy.context.scene.custom_props.items(), " end")
    for (k, v) in bpy.context.scene.custom_props.items():
        if "gvs_" in k or "Fisheye" in k:
            del bpy.context.scene.custom_props[k]
    # print(" Now custom prop ", bpy.context.scene.custom_props.items(), " end")

    # If launched in non-interactive mode
    if bpy.app.background:
        argv = sys.argv
        argv = argv[argv.index("--") + 1:]

        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--input_config', dest='config_file')
        args = parser.parse_known_args(argv)[0]
        output_path = os.path.dirname(args.config_file)

        apply_from_config(args.config_file)
        render_from_config(args.config_file)
