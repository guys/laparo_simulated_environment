import bpy



properties_endoscope = {"depth_endoscope_slider": bpy.props.FloatProperty(name='depth',
                                                                          default=-0.1,
                                                                          min=0,
                                                                          max=7,
                                                                          step=0.05)}


class Config_Endoscope(bpy.types.Panel):
    """ Config of the endoscope as a whole """
    
    bl_label = "Endoscope"
    bl_idname = "GVS_PT_endoscope"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_category = 'GVS'
    bl_options = {'DEFAULT_CLOSED'}
    
    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text="Position", icon="OBJECT_ORIGIN")
        
        # Change depth endoscope
        row = layout.row()
        row.label(text="Depth", icon="TRACKING_FORWARDS_SINGLE")
        row.operator('transform.depth_operator', text = 'Transform depth')
        row.prop(context.scene.custom_props, 'depth_endoscope_slider')
        
        
        
class Config_Endoscope_Camera(bpy.types.Panel):  
    """ Config of the endoscope camera, subcomponent of Config_Endoscope """
    
    bl_label = "Camera"
    bl_idname = "GVS_PT_endoscope_camera"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_parent_id = "GVS_PT_endoscope"
    bl_category = 'GVS'
    bl_options = {'DEFAULT_CLOSED'}
    
    camera = bpy.data.objects['Endoscope_camera'].data
    camera_dof = camera.dof
    
    # Led lights around the camera
    led_light_0 = bpy.data.objects['Endoscope_spot_0'].data 
    led_light_1 = bpy.data.objects['Endoscope_spot_1'].data
    led_light_2 = bpy.data.objects['Endoscope_spot_2'].data
    led_light_3 = bpy.data.objects['Endoscope_spot_3'].data
    led_light_4 = bpy.data.objects['Endoscope_spot_4'].data
    led_light_5 = bpy.data.objects['Endoscope_spot_5'].data
    led_light_6 = bpy.data.objects['Endoscope_spot_6'].data
    
    # Halo available if want endoscope light without the endoscope
    halo_fixe = bpy.data.objects['Light halo endoscope (fixe)'].data
    
    def draw(self, context):
        layout = self.layout
    
        # Intrinsic camera parameters
        row = layout.row()
        row.label(text="Intrinsic params", icon="OUTLINER_OB_CAMERA")
    
        row = layout.row()
        row.prop(self.camera, "lens_unit")
        row.prop(self.camera, "lens")
        row.prop(self.camera, "angle_x")
        row.prop(self.camera, "angle_y")
    
        row = layout.row()
        row.prop(self.camera, "clip_start")
        
        row = layout.column()
        row.prop(self.camera, "sensor_fit")
        
        row = layout.row()
        row.prop(self.camera, "sensor_height")
        
        row = layout.row()
        row.prop(self.camera, "sensor_width")
        
        row = layout.row()
        row.prop(self.camera_dof, "focus_distance")
        row.prop(self.camera_dof, "aperture_fstop")
        row = layout.row()
        
        # Power of led lighting
        row.label(text="LEDs powers", icon="OUTLINER_OB_LIGHT")
        row.prop(self.led_light_0, "energy")
        row.prop(self.led_light_1, "energy")
        row.prop(self.led_light_2, "energy")
        row.prop(self.led_light_3, "energy")
        row.prop(self.led_light_4, "energy")
        row.prop(self.led_light_5, "energy")
        row.prop(self.led_light_6, "energy")
        row = layout.row()
        row.label(text="Simulated halo (fixed spot)", icon="OUTLINER_OB_LIGHT")
        row.prop(self.halo_fixe, "energy")
        

class TRANSFORM_OT_DEPTH(bpy.types.Operator):
    """ Action of translating the endoscope along the axis of the multicamera GVS prototype """
    
    bl_label = "Move depth endoscope"
    bl_idname = 'transform.depth_operator'
    
    def execute(self, context):    
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects['Endoscope']
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        context.scene.transform_orientation_slots[0].type = 'LOCAL' 
        scene = context.scene
        bpy.ops.transform.translate(value=(scene.custom_props.depth_endoscope_slider, 0., 0.),
                                    orient_type='LOCAL',
                                    orient_matrix_type='LOCAL',
                                    constraint_axis=(True, False, False),
                                    mirror=True,
                                    use_proportional_edit=False,
                                    proportional_edit_falloff='SMOOTH',
                                    proportional_size=1,
                                    use_proportional_connected=False,
                                    use_proportional_projected=False)
        return {'FINISHED'}


to_be_registered = [Config_Endoscope, Config_Endoscope_Camera, TRANSFORM_OT_DEPTH]
