import bpy
from bpy.props import StringProperty, CollectionProperty
from tools import StringValue, StringCollection
import os

properties_rendering={
    "presets_resolutions_cameras": bpy.props.EnumProperty(
        items=(("misumi", "misumi", "misumi"),
               ("misumi_norot", "misumi_norot", "misumi_norot"),
               ("pi", "pi", "pi")), name="Preset resolution cameras"),
    #"endo_camera": bpy.props.BoolProperty(name="Endoscope camera",
    #                                      description="",
    #                                      default=False),
    #"gvs_camera_right_misumi": bpy.props.BoolProperty(name="GVS camera right misumi",
    #                                                  description="",
    #                                                  default=False),
    #"gvs_camera_right_misuminorot": bpy.props.BoolProperty(name="GVS camera right misuminorot",
    #                                                       description="",
    #                                                       default=False),
    #"gvs_camera_right_pi": bpy.props.BoolProperty(name="GVS camera right pi",
    #                                              description="",
    #                                              default=False),
    #"gvs_camera_left_misumi": bpy.props.BoolProperty(name="GVS camera left misumi",
    #                                                 description="",
    #                                                 default=False),
    #"gvs_camera_left_misuminorot": bpy.props.BoolProperty(name="GVS camera left misuminorot",
    #                                                      description="",
    #                                                      default=False),
    #"gvs_camera_left_pi": bpy.props.BoolProperty(name="GVS camera left pi",
    #                                             description="",
    #                                             default=False),
    "output_path": bpy.props.StringProperty(name="output_path",
                                            default="/tmp/",
                                            subtype="FILE_PATH")
}

# Rendre toutes les caméras de l'environnement disponibles
cams = [cam for cam in bpy.data.objects if cam.type == 'CAMERA' and "fake" not in cam.name]
print(cams)
#cams = [ i for i in bpy.data.objects if i.name in bpy.context.scene.custom_props and
#                 bpy.context.scene.custom_props[i.name]==1 ]
for c in cams:
    properties_rendering[c.name] = bpy.props.BoolProperty(name=c.name,
                                                          description="",
                                                          default=False)
print(properties_rendering)


class Config_Rendu(bpy.types.Panel):
    bl_label = "Rendering"
    bl_idname = "GVS_PT_rendu_gvs"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_category = 'GVS'
    bl_options = {'DEFAULT_CLOSED'}
    
    def draw(self, context):
        
        layout = self.layout
        row = layout.row()
        row.label(text="Render engine")
        row.prop(bpy.context.scene.render, "engine")
        
        row = layout.row()
        row.label(text="Render samples")
        if bpy.context.scene.render.engine == "CYCLES":
            row.prop(bpy.context.scene.cycles, "samples")
        else:
            row.prop(bpy.context.scene.eevee, "taa_render_samples")
        
        row = layout.row()
        row.label(text="Exposure")
        row.prop(bpy.data.scenes["Scene"].view_settings, "exposure")
        
        row = layout.row()
        row.label(text="Preset resolution")
        row.operator('transform.select_preset', text = 'Select preset')
        row.prop(context.scene.custom_props, 'presets_resolutions_cameras')
        
        row = layout.row()
        row.label(text="Resolution X")
        row.prop(bpy.data.scenes["Scene"].render, "resolution_x")
        
        row = layout.row()
        row.label(text="Resolution Y")
        row.prop(bpy.data.scenes["Scene"].render, "resolution_y")
        
        row = layout.row()
        row.label(text="Bake lighting (if some irradiance volumes")
        row.operator('render.bake_lighting', text='Bake lighting')
        
        row = layout.row()

        cams = [cam for cam in bpy.data.objects if cam.type == 'CAMERA']
        for c in cams:
            layout.prop(context.scene.custom_props, c.name, text=c.name)
        #layout.prop(context.scene.custom_props, "gvs_camera_right_misumi", text="gvs_camera_right_misumi")
        #layout.prop(context.scene.custom_props, "gvs_camera_right_misuminorot", text="gvs_camera_right_misuminorot")
        #layout.prop(context.scene.custom_props, "gvs_camera_right_pi", text="gvs_camera_right_pi")
        #layout.prop(context.scene.custom_props, "gvs_camera_left_misumi", text="gvs_camera_left_misumi")
        #layout.prop(context.scene.custom_props, "gvs_camera_left_misuminorot", text="gvs_camera_left_misuminorot")
        #layout.prop(context.scene.custom_props, "gvs_camera_left_pi", text="gvs_camera_left_pi")
        layout.prop(context.scene.custom_props, "output_path", text="Path where to save")
        row.label(text="Render everything", icon="SCENE")
        row.operator('render.my_render', text='Start')


########################################
######### RENDERING ACTION  ############
########################################       
        
class RENDU_OPERATOR(bpy.types.Operator):
    """ Render images from selected cameras """
    
    bl_label = "Render all images"
    bl_idname = 'render.my_render'
    
    def execute(self, context):  
        cameras = []
        # Check the cameras that are selected to be rendered
        #print("Custom prop ", bpy.context.scene.custom_props.items(), " end")
        cams = [i for i in bpy.data.objects if i.name in bpy.context.scene.custom_props and
                bpy.context.scene.custom_props[i.name] == 1]
        print("LIST CAMS", cams)
        for i, c in enumerate(cams):
            if c.name in context.scene.custom_props:
                cameras.append({"name": str(i), "value":c.name})
        #if context.scene.custom_props.gvs_camera_right_misumi:
        #    cameras.append({"name":"1", "value":"Camera.Misumi_right"})
        #if context.scene.custom_props.gvs_camera_left_misumi:
        #    cameras.append({"name":"2", "value":"Camera.Misumi_left"})
        #if context.scene.custom_props.gvs_camera_right_misuminorot:
        #    cameras.append({"name":"3", "value":"Camera.Misumi_right_norot"})
        #if context.scene.custom_props.gvs_camera_left_misuminorot:
        #    cameras.append({"name":"4", "value":"Camera.Misumi_left_norot"})
        #if context.scene.custom_props.gvs_camera_right_pi:
        #    cameras.append({"name":"5", "value":"Camera.Pi_right"})
        #if context.scene.custom_props.gvs_camera_left_pi:
        #    cameras.append({"name":"6", "value":"Camera.Pi_left"})
        #cameras.append({"name":"7", "value":"Endoscope_camera"})
        matConf = []
    
        cfg = { "name":"1", "value": [{"name":"__config_name", "value":""},]}
        matConf.append(cfg)
        bpy.ops.render.everything_non_modal(basePath=os.path.dirname(__file__)+"\\"
                                                     +context.scene.custom_props.output_path+"\\",
                                            camerasList=cameras, materialCfg=matConf)
        return {'FINISHED'}    
        
        
class BAKE_LIGHTING_OPERATOR(bpy.types.Operator):
    bl_label = "Bake lighting"
    bl_idname = 'render.bake_lighting'
    
    def execute(self, context): 
        try: 
            bpy.ops.scene.light_cache_free()
        except Exception as e:
            self.report({'INFO'}, "The baking cache was empty")
        bpy.ops.scene.light_cache_bake()
        return {'FINISHED'}          
        
        
        
#class Multi_Render(bpy.types.Operator):
#    """ Render with non-GUI blender """
#
#    bl_idname = "render.everything"
#    bl_label = "Render Everything!"
#
#    #string - Base path to render to..
#    basePath: StringProperty(name="Path")
#
#    #List of cameras to render
#    camerasList: CollectionProperty(type=StringValue, name="Cameras")
#
#    #List of material configurations to render
#    materialCfg: CollectionProperty(type=StringCollection, name="Materials")
#
#    #####################
#    # internal variables
#    cancelRender = None     #was render cancelled by user
#    rendering = None        #is currently rendering
#    renderQueue = None      #render queue
#    timerEvent = None       #timer
#
#    #Rendering callback functions
#    def pre_render(self, dummy1, dummy2):
#        self.rendering = True    #mark rendering flag
#
#    def post_render(self, dummy1, dummy2):
#        #self.report({"INFO"},"post render")
#        self.renderQueue.pop(0) #remove finished item from render queue
#        self.rendering = False  #clear rendering flag
#
#    def on_render_cancel(self, dummy):
#        self.cancelRender = True    #mark cancel render flag
#
#    #Main operator function for user execution
#    def execute(self, context):
#
#        self.cancelRender = False   # clear cancel flag
#        self.rendering = False      # clear rendering flag
#
#
#        #fill renderQueue from input parameters with each camera rendering each material configuration
#        self.renderQueue = []
#        for mat in self.materialCfg:
#            for cam in self.camerasList:
#                self.renderQueue.append({"Camera":cam.value, "MatCfg":mat.value})
#
#
#        #Register callback functions
#        bpy.app.handlers.render_pre.append(self.pre_render)
#        bpy.app.handlers.render_post.append(self.post_render)
#        bpy.app.handlers.render_cancel.append(self.on_render_cancel)
#
#        #Create timer event that runs every second to check if render renderQueue needs to be updated
#        self.timerEvent = context.window_manager.event_timer_add(5., window=context.window)
#
#        #register this as running in background
#        context.window_manager.modal_handler_add(self)
#        #self.report({"INFO"}, "Execute: " + configName)
#        return {"RUNNING_MODAL"}
#
#    #modal callback when there is some event..
#    def modal(self, context, event):
#        print("Modal callback")
#        #timer event every second
#        if event.type == 'TIMER':
#
#            # If cancelled or no items in queue to render, finish.
#            #if True in (not self.renderQueue, self.cancelRender is True):
#            if not self.renderQueue or self.cancelRender is True:
#
#                #remove all render callbacks
#                bpy.app.handlers.render_pre.remove(self.pre_render)
#                bpy.app.handlers.render_post.remove(self.post_render)
#                bpy.app.handlers.render_cancel.remove(self.on_render_cancel)
#
#                #remove timer
#                context.window_manager.event_timer_remove(self.timerEvent)
#
#                self.report({"INFO"},"RENDER QUEUE FINISHED")
#
#                return {"FINISHED"}
#
#            #nothing is rendering and there are items in queue
#            elif self.rendering is False:
#
#                sc = bpy.context.scene
#                qitem = self.renderQueue[0] #first item in rendering queue
#
#                #change scene active camera
#                cameraName = qitem["Camera"]
#                if cameraName in sc.objects:
#                    sc.camera = bpy.data.objects[qitem["Camera"]]
#                else:
#                    self.report({'ERROR_INVALID_INPUT'}, message="Can not find camera "+cameraName+" in scene!")
#                    return {'CANCELLED'}
#
#
#                matCfg = qitem["MatCfg"]
#                # for simplcity we store special key __config_name along material assignments to store name for this material configuration
#                configName = matCfg["__config_name"].value
#
#                self.report({"INFO"}, "Rendering config: " + configName)
#
#                #set output file path as base path + condig name + camera name
#                sc.render.filepath = self.basePath + configName + "_" + sc.camera.name
#                self.report({"INFO"}, "Out Path: " + sc.render.filepath)
#
#
#                #Go through and apply material configs
#                for kc in matCfg:
#                    if kc.name == "__config_name":
#                        continue
#
#                    objName = kc.name
#                    matName = kc.value
#
#                    obj = None
#
#                    if objName in sc.objects:
#                        obj = bpy.data.objects[objName]
#                    else:
#                        self.report({'ERROR_INVALID_INPUT'}, message="Can not find object "+objName+" in scene!")
#
#                    if matName in bpy.data.materials and obj is not None:
#                        mat = bpy.data.materials[matName]
#                        obj.material_slots[0].material = mat #set as fist material .. will not work with multiple materials
#                    else:
#                        self.report({'ERROR_INVALID_INPUT'}, message="Can not find material "+matName+" in scene!")
#
#                #start new render
#                #bpy.ops.render.render("INVOKE_DEFAULT", write_still=True)
#                bpy.ops.render.render(write_still=True)
#
#        return {"PASS_THROUGH"}
        

class Multi_Render_Non_Modal(bpy.types.Operator):
    """ Render with GUI blender """ 
    
    bl_idname = "render.everything_non_modal"
    bl_label = "Render Everything in a non modal way!"

    #string - Base path to render to..
    basePath: StringProperty(name="Path")
    
    #List of cameras to render 
    camerasList: CollectionProperty(type=StringValue, name="Cameras")

    #List of material configurations to render
    materialCfg: CollectionProperty(type=StringCollection, name="Materials") 
    
    #####################
    # internal variables
    renderQueue = None      #render queue 
    timerEvent = None       #timer
                    
    #fill renderQueue from input parameters with each camera rendering each material configuration          
    def execute(self, context):
        print("Camera list", [i for i in self.camerasList], " end")
        self.renderQueue = []
        for mat in self.materialCfg:
            for cam in self.camerasList:
                print("test ", cam.value, mat.value)
                self.renderQueue.append({"Camera": cam.value, "MatCfg": mat.value})
            
        for i in self.renderQueue:         
            sc = bpy.context.scene
            qitem = i 
            
            #change scene active camera
            cameraName = qitem["Camera"]
            if cameraName in sc.objects:
                sc.camera = bpy.data.objects[qitem["Camera"]]
                print(sc.camera)
                print(sc.camera.data.cycles.panorama_type)
            else:
                self.report({'ERROR_INVALID_INPUT'}, message="Can not find camera "+cameraName+" in scene!")
                return {'CANCELLED'}
                
            matCfg = qitem["MatCfg"]
            # for simplcity we store special key __config_name along material assignments to store name for this material configuration
            configName = matCfg["__config_name"].value
            
            self.report({"INFO"}, "Rendering config: " + configName)

            #set output file path as base path + condig name + camera name
            sc.render.filepath = self.basePath + configName + "_" + sc.camera.name
            self.report({"INFO"}, "Out Path: " + sc.render.filepath)
            
            #Go through and apply material configs
            for kc in matCfg:
                if kc.name == "__config_name":
                    continue
                
                objName = kc.name
                matName = kc.value
                
                obj = None
                
                if objName in sc.objects:
                    obj = bpy.data.objects[objName]
                else:
                    self.report({'ERROR_INVALID_INPUT'}, message="Can not find object "+objName+" in scene!")
                    
                if matName in bpy.data.materials and obj is not None:
                    mat = bpy.data.materials[matName]
                    obj.material_slots[0].material = mat #set as fist material .. will not work with multiple materials
                else:
                    self.report({'ERROR_INVALID_INPUT'}, message="Can not find material "+matName+" in scene!")
                
            #start new render                
            bpy.ops.render.render(write_still=True)


        # At the end of the loop
        self.report({"INFO"},"RENDER QUEUE FINISHED")
        return {'FINISHED'}


######################################################
######### CAMERA PRESET SELECTION  ACTION ############
######################################################         
                  
class OPERATOR_SELECT_PRESET(bpy.types.Operator):
    """ Camera resolution selection depending on a preset of camera (misumi or raspberry pi camera """
    bl_label = "Select preset camera"
    bl_idname = 'transform.select_preset'
    
    def execute(self, context):    
        if context.scene.custom_props.presets_resolutions_cameras == "misumi" or context.scene.custom_props.presets_resolutions_cameras == "misumi_norot":
            # Misumi cameras
            bpy.data.scenes["Scene"].render.resolution_x = 1280
            bpy.data.scenes["Scene"].render.resolution_y = 720
        else:
            # Raspberry pi cameras
            bpy.data.scenes["Scene"].render.resolution_x = 1920
            bpy.data.scenes["Scene"].render.resolution_y = 1080
        return {'FINISHED'}   
        

to_be_registered = [Config_Rendu, RENDU_OPERATOR, BAKE_LIGHTING_OPERATOR, Multi_Render_Non_Modal,
                    OPERATOR_SELECT_PRESET]
