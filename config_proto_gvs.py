import bpy
from mathutils import Euler

# GVS prototype
properties_gvs_prototype={"position_proto_slider": bpy.props.FloatVectorProperty(name='position',
                                                                                 default=(0.29379, 0.76016, -0.86941),
                                                                                 min=-1.0,
                                                                                 max=1.0),
                          "orientation_proto_slider": bpy.props.FloatVectorProperty(name='orientation',
                                                                                    default=(6.25863661436,
                                                                                             5.96222630718,
                                                                                             6.05926861436),
                                                                                    min=0,
                                                                                    max=7),
                          "orientation_cams_proto": bpy.props.FloatVectorProperty(name='orientation',
                                                                                  default=(0, 0, 0)),
                          "distance_intercams_proto_slider": bpy.props.FloatProperty(name='distance',
                                                                                     default=0.005,
                                                                                     step=0.001,
                                                                                     precision=5),
                          "camera_to_modify": bpy.props.EnumProperty(
                                        items=(("Camera.Misumi_right", "Camera.Misumi_right", "Camera.Misumi_right"),
                                               ("Camera.Misumi_right_norot", "Camera.Misumi_right_norot",
                                                "Camera.Misumi_right_norot"),
                                               ("Camera.Pi_right", "Camera.Pi_right", "Camera.Pi_right"),
                                               ("Camera.Misumi_left", "Camera.Misumi_left", "Camera.Misumi_left"),
                                               ("Camera.Misumi_left_norot", "Camera.Misumi_left_norot",
                                                "Camera.Misumi_left_norot"),
                                               ("Camera.Pi_left", "Camera.Pi_left", "Camera.Pi_left"),
                                               #("Camera.Fisheye_left", "Camera.Fisheye_left", "Camera.Fisheye_left"),
                                               #("Camera.Fisheye_right", "Camera.Fisheye_right", "Camera.Fisheye_right")
                                               ),
                          name="camera")
}


class Config_Proto_GVS(bpy.types.Panel):
    """ Multi-cameras prototype (GVS) config as a whole """
    
    bl_label = "Prototype GVS"
    bl_idname = "GVS_PT_proto"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_category = 'GVS'
    bl_options = {'DEFAULT_CLOSED'}
    
    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text="Position", icon="OBJECT_ORIGIN")
        row = layout.row()
        row.operator('transform.position_proto_operator', text = 'Transform position')
        row.prop(context.scene.custom_props, 'position_proto_slider')
        
        row = layout.row()
        row.label(text="Orientation", icon="ORIENTATION_LOCAL")
        row = layout.row()
        row.operator('transform.orientation_proto_operator', text = 'Transform orientation')
        row.prop(context.scene.custom_props, 'orientation_proto_slider')
        
        row = layout.row()
        row.label(text="Select a camera to modify")
        row.prop(context.scene.custom_props, 'camera_to_modify')



class Config_ProtoGVS_Camera(bpy.types.Panel): 
    """ Cameras of multicam prototype configuration, subcomponent of  GVS_PT_proto """
    
    bl_label = "Camera"
    bl_idname = "GVS_PT_protogvs_camera"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_parent_id = "GVS_PT_proto"
    bl_category = 'GVS'
    bl_options = {'DEFAULT_CLOSED'} 
    camera = bpy.data.objects["Camera.Misumi_right"].data # default camera
    
    # LEDs ligthning around the two cameras
    led_light_1 = bpy.data.objects['GVS_cam_1_spot_1'].data
    led_light_2 = bpy.data.objects['GVS_cam_1_spot_2'].data
    led_light_3 = bpy.data.objects['GVS_cam_1_spot_3'].data
    led_light_4 = bpy.data.objects['GVS_cam_1_spot_4'].data
    led_light_5 = bpy.data.objects['GVS_cam_1_spot_5'].data
    led_light_6 = bpy.data.objects['GVS_cam_1_spot_6'].data
    
    led_light_7 = bpy.data.objects['GVS_cam_2_spot_1'].data
    led_light_8 = bpy.data.objects['GVS_cam_2_spot_2'].data
    led_light_9 = bpy.data.objects['GVS_cam_2_spot_3'].data
    led_light_10 = bpy.data.objects['GVS_cam_2_spot_4'].data
    led_light_11 = bpy.data.objects['GVS_cam_2_spot_5'].data
    led_light_12 = bpy.data.objects['GVS_cam_2_spot_6'].data
    
    def draw(self, context):
        scene = context.scene 
        self.camera = bpy.data.objects[scene.custom_props.camera_to_modify].data 
        layout = self.layout
        
        # Intrinsic camera parameters
        row = layout.row()
        row.label(text="Intrinsic parameters", icon="OUTLINER_OB_CAMERA")
    
        row = layout.row()
        row.prop(self.camera, "type")
        print(self.camera.type)
        if self.camera.type == 'PERSP':
            row.prop(self.camera, "lens_unit")
            row.prop(self.camera, "lens")
            row.prop(self.camera, "angle_x")
            row.prop(self.camera, "angle_y")
    
            row = layout.row()
            row.prop(self.camera, "clip_start")

            row = layout.column()
            row.prop(self.camera, "sensor_fit")

            row = layout.row()
            row.prop(self.camera, "sensor_height")

            row = layout.row()
            row.prop(self.camera, "sensor_width")

            row = layout.row()
            self.camera_dof = self.camera.dof
            row.prop(self.camera_dof, "focus_distance")
            row.prop(self.camera_dof, "aperture_fstop")
            row = layout.row()
        
        # Power of LEDs
        row.label(text="LEDs powers", icon="OUTLINER_OB_LIGHT")
        row.prop(self.led_light_1, "energy")
        row.prop(self.led_light_2, "energy")
        row.prop(self.led_light_3, "energy")
        row.prop(self.led_light_4, "energy")
        row.prop(self.led_light_5, "energy")
        row.prop(self.led_light_6, "energy")   
        row = layout.row()
        row.prop(self.led_light_7, "energy")
        row.prop(self.led_light_8, "energy")
        row.prop(self.led_light_9, "energy")
        row.prop(self.led_light_10, "energy")
        row.prop(self.led_light_11, "energy")
        row.prop(self.led_light_12, "energy")   

        row = layout.row()
        row.operator('transform.orientation_camselected_proto_operator', text='Transform orient')
        row.prop(context.scene.custom_props, 'orientation_cams_proto')
        
        row = layout.row()
        row.operator('transform.interdistance_cams_proto_operator', text='Interdistance cameras')
        row.prop(context.scene.custom_props, 'distance_intercams_proto_slider')

#######################################################
######### MULTICAM (GVS) PROTOTYPE ACTION  ############
#######################################################      
        
class TRANSFORM_OT_POSITION_PROTO(bpy.types.Operator):
    """ Translate the multicamera prototype """
    
    bl_label = "Change position ot the proto GVS"
    bl_idname = 'transform.position_proto_operator'
    
    def execute(self, context):  
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects['GVS']
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        scene = context.scene  
        context.scene.transform_orientation_slots[0].type = 'GLOBAL'
        obj.location = scene.custom_props.position_proto_slider
        return {'FINISHED'}
        
        
class TRANSFORM_OT_ORIENTATION_PROTO(bpy.types.Operator):
    """ Rotate the camera prototype """
    
    bl_label = "Change orientation of the proto GVS"
    bl_idname = 'transform.orientation_proto_operator'
    
    def execute(self, context):  
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects['GVS']
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        scene = context.scene  
        context.scene.transform_orientation_slots[0].type = 'GLOBAL'
        obj.rotation_euler = Euler(scene.custom_props.orientation_proto_slider, 'XYZ')
        return {'FINISHED'}     
        
        
class TRANSFORM_OT_ORIENTATION_PROTO_CAMERA_SELECTED(bpy.types.Operator):
    bl_label = "Change orientation of the left camera of the proto GVS"
    bl_idname = 'transform.orientation_camselected_proto_operator'
    
    def execute(self, context): 
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects[context.scene.custom_props.camera_to_modify]
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        scene = context.scene  
        context.scene.transform_orientation_slots[0].type = 'LOCAL'
        
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
        bpy.ops.transform.rotate(value=scene.custom_props.orientation_cams_proto[0], 
                                 orient_axis='X', orient_type='LOCAL')
        bpy.ops.transform.rotate(value=scene.custom_props.orientation_cams_proto[1], 
                                 orient_axis='Y', orient_type='LOCAL')
        bpy.ops.transform.rotate(value=scene.custom_props.orientation_cams_proto[2], 
                                 orient_axis='Z', orient_type='LOCAL')
    
        return {'FINISHED'}   
    
    
class TRANSFORM_OT_INTERDISTANCE_PROTO_CAMERAS(bpy.types.Operator):
    bl_label = "Change the inter-distance between the two cameras"
    bl_idname = 'transform.interdistance_cams_proto_operator'
    
    def execute(self, context):  
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects[context.scene.custom_props.camera_to_modify]
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        scene = context.scene  
        context.scene.transform_orientation_slots[0].type = 'LOCAL'
        
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
        bpy.ops.transform.translate(value=(scene.custom_props.distance_intercams_proto_slider, 0, 0),
                        orient_type='LOCAL')    
        return {'FINISHED'}   


to_be_registered = [Config_Proto_GVS, Config_ProtoGVS_Camera, TRANSFORM_OT_POSITION_PROTO,
                    TRANSFORM_OT_INTERDISTANCE_PROTO_CAMERAS, TRANSFORM_OT_ORIENTATION_PROTO,
                    TRANSFORM_OT_ORIENTATION_PROTO_CAMERA_SELECTED]

