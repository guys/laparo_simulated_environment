import bpy
import math

properties_fisheye = {"rotation_fisheye_slider2": bpy.props.FloatProperty(name='Angle',
                                                                          default=15.,
                                                                          min=-180.,
                                                                          max=180.,
                                                                          step=10.),
                      "camera_to_modify_fisheye": bpy.props.EnumProperty(
                                        items=(
                                               ("Camera.Fisheye_left", "Camera.Fisheye_left", "Camera.Fisheye_left"),
                                               ("Camera.Fisheye_right", "Camera.Fisheye_right", "Camera.Fisheye_right")),
                                        name="camera"),
                      "translate_fisheye_slider": bpy.props.FloatProperty(name='distance',
                                                                          default=0.004,
                                                                          step=0.001,
                                                                          precision=5),
                      }


class Config_Fisheye(bpy.types.Panel):
    """ Config of the fisheye model """

    bl_label = "Fisheye"
    bl_idname = "FISHEYE_PT"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_category = 'GVS'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        # Change angle fisheye cameras
        row = layout.row()
        row.label(text="Select a camera to modify")
        row.prop(context.scene.custom_props, 'camera_to_modify_fisheye')

        row = layout.row()
        row.label(text="Angle", icon="FILE_REFRESH")
        row.operator('transform.orientation_fisheye', text='Rotate fisheye cameras')
        row.prop(context.scene.custom_props, 'rotation_fisheye_slider2')

        row = layout.row()
        row.label(text="Translate", icon="EMPTY_SINGLE_ARROW")
        row.operator('transform.translate_fisheye_operator', text='Interdistance cameras')
        row.prop(context.scene.custom_props, 'translate_fisheye_slider')


class TRANSFORM_OT_ORIENTATION_CAMERAS(bpy.types.Operator):
    """ Rotate fisheye cameras """

    bl_label = "Rotate the fisheye cameras around the endoscope axis"
    bl_idname = 'transform.orientation_fisheye'

    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')

        #bpy.context.scene.objects["Camera.Fisheye_left_fake"].hide_viewport=True
        #bpy.context.scene.objects["Camera.Fisheye_left_fake"].hide_render=True
        #bpy.context.scene.objects["Camera.Fisheye_right_fake"].hide_viewport=True
        #bpy.context.scene.objects["Camera.Fisheye_right_fake"].hide_render=True

        if context.scene.custom_props.camera_to_modify_fisheye == "Camera.Fisheye_left":
            bpy.context.scene.objects["Camera.Fisheye_left"].select_set(True)
            bpy.context.scene.objects["Camera.Fisheye_left_fake"].select_set(True)
            bpy.context.view_layer.objects.active = bpy.context.scene.objects["Camera.Fisheye_left"]
            scene = context.scene
            context.scene.transform_orientation_slots[0].type = 'LOCAL'
            bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
            bpy.ops.transform.rotate(value=math.radians(scene.custom_props.rotation_fisheye_slider2),
                                     orient_axis='X', orient_type='LOCAL')

        elif context.scene.custom_props.camera_to_modify_fisheye == "Camera.Fisheye_right":
            bpy.context.scene.objects["Camera.Fisheye_right"].select_set(True)
            bpy.context.scene.objects["Camera.Fisheye_right_fake"].select_set(True)
            bpy.context.view_layer.objects.active = bpy.context.scene.objects["Camera.Fisheye_right"]
            scene = context.scene
            context.scene.transform_orientation_slots[0].type = 'LOCAL'
            bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
            bpy.ops.transform.rotate(value=math.radians(scene.custom_props.rotation_fisheye_slider2),
                                     orient_axis='X', orient_type='LOCAL')
        else:
            assert(False)


        return {'FINISHED'}



#class TRANSFORM_OT_ORIENTATION_CAMERAS(bpy.types.Operator):
#    """ Rotate fisheye cameras """
#
#    bl_label = "Rotate the fisheye cameras around the endoscope axis"
#    bl_idname = 'transform.orientation_fisheye'
#
#    def __init__(self):
#        #bpy.context.scene.objects["Camera.Fisheye_left"].select_set(True)
#        #bpy.context.scene.objects["Camera.Fisheye_right"].select_set(True)
#        #loc1 = bpy.context.selected_objects[1].matrix_world.translation
#        #loc0 = bpy.context.selected_objects[0].matrix_world.translation
#        #self.mean = (loc0+loc1)/2.
#        #self.mean = (0.5302, 0.9306, -0.8610)
#        #self.mean = (0.28561368584632874, 0.5334489345550537, -0.8359281420707703)
#        pass
#
#    def execute(self, context):
#        bpy.ops.object.select_all(action='DESELECT')
#        bpy.context.scene.objects["Camera.Fisheye_left"].select_set(True)
#        bpy.context.scene.objects["Camera.Fisheye_right"].select_set(True)
#        bpy.context.view_layer.objects.active = bpy.context.scene.objects["Camera.Fisheye_right"]
#
#        saved_position = bpy.context.scene.objects["Camera.Fisheye_left"].location
#        saved_orientation = bpy.context.scene.objects["Camera.Fisheye_left"].rotation_euler
#        ##bpy.context.view_layer.objects.active = obj
#        #context.scene.transform_orientation_slots[0].type = 'LOCAL'
#        #bpy.context.scene.transform_orientation_slots[0].custom_orientation.matrix.to_4x4()
#        #loc1 = bpy.context.selected_objects[1].matrix_world.translation
#        #loc0 = bpy.context.selected_objects[0].matrix_world.translation
#        #bpy.context.scene.objects["Camera.Fisheye_right"].select_set(False)
#
#        scene = context.scene
#        #bpy.ops.transform.create_orientation(name="Frank", overwrite=True)
#        #orientation = scene.orientations.get("Frank")
#
#
#        #bpy.ops.transform.rotate(value=scene.custom_props.rotation_fisheye_slider2,
#        #                         orient_axis='X', orient_type=orientation) #'LOCAL')
#
#
#
#        ## TMP
#        ## Create view for manual alignment along baseline.
#        #bpy.ops.transform.create_orientation(name="BASELINE", overwrite=True)
#        #### Set baseline
#        #slot = bpy.context.scene.transform_orientation_slots[0]
#        ## Save current orientation setting
#        #last_slot = slot.type
#        ## Set new orientation (custom_orientation isn't available until we set the
#        ## type to a custom orientation)
#        #slot.type = 'BASELINE'
#        #from math import radians
#        #from mathutils import Matrix
#        #m = Matrix.Rotation(radians(45), 3, (1, 1, 1))
#        #slot.custom_orientation.matrix = m
#        ## Set orientation back to what it was
#        #slot.type = last_slot
#        ## END TMP
#        bpy.ops.transform.rotate(value=scene.custom_props.rotation_fisheye_slider2,
#                                orient_axis='X', orient_type='LOCAL') #, center_override=self.mean) #LOCAL')
#
#
#        # Reset
#        bpy.context.scene.objects["Camera.Fisheye_left"].location = saved_position
#        bpy.context.scene.objects["Camera.Fisheye_left"].rotation_euler = saved_orientation
#
#        #bpy.context.view_layer.objects.active = obj
#        #scene = context.scene
#        #context.scene.transform_orientation_slots[0].type = 'local_pince_right'
#        #bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
#        #obj.location = scene.custom_props.position_forceps_right_slider
#        return {'FINISHED'}


class Config_Fisheye_Camera(bpy.types.Panel):
    """ Cameras of multicam prototype configuration, subcomponent of  GVS_PT_proto """

    bl_label = "Camera"
    bl_idname = "GVS_PT_fisheye_camera"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_parent_id = "FISHEYE_PT"
    bl_category = 'GVS'
    bl_options = {'DEFAULT_CLOSED'}
    camera = bpy.data.objects["Camera.Fisheye_left"].data  # default camera

    def draw(self, context):
        scene = context.scene
        self.camera = bpy.data.objects[scene.custom_props.camera_to_modify_fisheye].data
        layout = self.layout

        # Intrinsic camera parameters
        row = layout.row()

        row.label(text="Intrinsic parameters", icon="OUTLINER_OB_CAMERA")

        row = layout.row()
        row.prop(self.camera, "type")
        row.prop(self.camera.cycles, "fisheye_fov")
        row.prop(self.camera.cycles, "panorama_type")
        if self.camera.cycles.panorama_type=="FISHEYE_EQUISOLID":
            row.prop(self.camera.cycles, "fisheye_lens")
        #print(self.camera.type)
        #if self.camera.type == 'PERSP':
        #    row.prop(self.camera, "lens_unit")
        #    row.prop(self.camera, "lens")
        #    row.prop(self.camera, "angle_x")
        #    row.prop(self.camera, "angle_y")

        #    row = layout.row()
        #    row.prop(self.camera, "clip_start")

        #    row = layout.column()
        #    row.prop(self.camera, "sensor_fit")

        #    row = layout.row()
        #    row.prop(self.camera, "sensor_height")

        #    row = layout.row()
        #    row.prop(self.camera, "sensor_width")

        #    row = layout.row()
        #    self.camera_dof = self.camera.dof
        #    row.prop(self.camera_dof, "focus_distance")
        #    row.prop(self.camera_dof, "aperture_fstop")
        #    row = layout.row()





class TRANSFORM_OT_TRANSLATE_FISHEYE_CAMERAS(bpy.types.Operator):
    bl_label = "Translate the fisheye cameras"
    bl_idname = 'transform.translate_fisheye_operator'

    def execute(self, context):
        if context.scene.custom_props.camera_to_modify_fisheye == "Camera.Fisheye_left":
            bpy.ops.object.select_all(action='DESELECT')
            obj = bpy.data.objects['Camera.Fisheye_left']
            obj.select_set(True)
            bpy.context.view_layer.objects.active = obj
            scene = context.scene
            context.scene.transform_orientation_slots[0].type = 'LOCAL'

            bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
            bpy.ops.transform.translate(value=(scene.custom_props.translate_fisheye_slider, 0, 0),
                                        orient_type='LOCAL')

            bpy.ops.object.select_all(action='DESELECT')
            obj = bpy.data.objects['Camera.Fisheye_left_fake']
            obj.select_set(True)
            bpy.context.view_layer.objects.active = obj
            scene = context.scene
            context.scene.transform_orientation_slots[0].type = 'LOCAL'

            bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
            bpy.ops.transform.translate(value=(scene.custom_props.translate_fisheye_slider, 0, 0),
                                        orient_type='LOCAL')
        elif context.scene.custom_props.camera_to_modify_fisheye == "Camera.Fisheye_right":
            bpy.ops.object.select_all(action='DESELECT')
            obj = bpy.data.objects['Camera.Fisheye_right']
            obj.select_set(True)
            bpy.context.view_layer.objects.active = obj
            scene = context.scene
            context.scene.transform_orientation_slots[0].type = 'LOCAL'

            bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
            bpy.ops.transform.translate(value=(scene.custom_props.translate_fisheye_slider, 0, 0),
                                        orient_type='LOCAL')

            bpy.ops.object.select_all(action='DESELECT')
            obj = bpy.data.objects['Camera.Fisheye_right_fake']
            obj.select_set(True)
            bpy.context.view_layer.objects.active = obj
            scene = context.scene
            context.scene.transform_orientation_slots[0].type = 'LOCAL'

            bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
            bpy.ops.transform.translate(value=(scene.custom_props.translate_fisheye_slider, 0, 0),
                                        orient_type='LOCAL')
        else:
            assert(False)

        return {'FINISHED'}














to_be_registered = [TRANSFORM_OT_ORIENTATION_CAMERAS, Config_Fisheye, Config_Fisheye_Camera, TRANSFORM_OT_TRANSLATE_FISHEYE_CAMERAS]