for path in ./*; do
    [ -d "${path}" ] || continue # if not a directory, skip
    dirname="$(basename "${path}")"
    blender ../../simulated_environment_free_final4.blend -b -P ../../moduleGVS.py -- -i $path/config_scene.json
done


