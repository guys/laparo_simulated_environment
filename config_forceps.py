import bpy
from mathutils import Euler


class Config_Forceps(bpy.types.Panel):
    """ Left forceps """
    
    bl_label = "A forceps"
    bl_idname = "GVS_PT_forceps"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = ""
    bl_options = {'DEFAULT_CLOSED'}

    def draw_(self, context, position_operator_name, position_slider_name, orientation_operator_name,
              orientation_slider_name):
        layout = self.layout
        
        # Change position
        row = layout.row()
        row.label(text="Position", icon="OBJECT_ORIGIN")
        row = layout.row()
        row.operator(position_operator_name, text='Transform position')
        row.prop(context.scene.custom_props, position_slider_name)

        # Change orientation
        row = layout.row()
        row.label(text="Orientation", icon="ORIENTATION_LOCAL")
        row = layout.row()
        row.operator(orientation_operator_name, text='Transform orientation')
        row.prop(context.scene.custom_props, orientation_slider_name)
 

properties_left_forceps={"orientation_forceps_left_slider": bpy.props.FloatVectorProperty(name='orientation',
                                                                                          default=(5.34, 0.28, 5.62),
                                                                                          min=0,
                                                                                          max=7),
                         "position_forceps_left_slider": bpy.props.FloatVectorProperty(name='position',
                                                                                       default=(0.12, 0.80, -0.77),
                                                                                       min=-1,
                                                                                       max=1)
                         }


class Config_Forceps_Left(Config_Forceps):
    """ Left forceps """
    bl_label = "Left forceps"
    bl_idname = "GVS_PT_forceps_left"
    bl_category = 'GVS'
    bl_options = {'DEFAULT_CLOSED'}

    def __init__(self):
        pass

    def draw(self, context):
        self.draw_(context, 'transform.position_forceps_left_operator', 'position_forceps_left_slider',
                   'transform.orientation_forceps_left_operator', 'orientation_forceps_left_slider')


# Right forceps
properties_right_forceps={"position_forceps_right_slider": bpy.props.FloatVectorProperty(name='position',
                                                                                         default=(0.46, 0.8, -0.78),
                                                                                         min=-1,
                                                                                         max=1),
                          "orientation_forceps_right_slider": bpy.props.FloatVectorProperty(name='orientation',
                                                                                            default=(5.76, 0.086, 4.14),
                                                                                            min=0,
                                                                                            max=7)
                          }



class Config_Forceps_Right(Config_Forceps):
    """ Right forceps """
    bl_label = "Right forceps"
    bl_idname = "GVS_PT_forceps_right"
    bl_category = 'GVS'
    bl_options = {'DEFAULT_CLOSED'}

    def __init__(self):
        pass

    def draw(self, context):
        self.draw_(context, 'transform.position_forceps_right_operator', 'position_forceps_right_slider',
                   'transform.orientation_forceps_right_operator', 'orientation_forceps_right_slider')


###########################################
######### LEFT FORCEPS ACTION  ############
###########################################        
        
class TRANSFORM_OT_ORIENTATION_FORCEPS_LEFT(bpy.types.Operator):
    """ Rotate left forceps """
    
    bl_label = "Change orientation laparoscopic forceps"
    bl_idname = 'transform.orientation_forceps_left_operator'
    
    def execute(self, context): 
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects['Pince_laparo_2']
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj 
        scene = context.scene  
        context.scene.transform_orientation_slots[0].type = 'GLOBAL'
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')       
        obj.rotation_euler = Euler(scene.custom_props.orientation_forceps_left_slider, 'XYZ')
        return {'FINISHED'}    
    
    
class TRANSFORM_OT_POSITION_FORCEPS_LEFT(bpy.types.Operator):
    """ Translate left forceps """
    
    bl_label = "Change position laparoscopic forceps"
    bl_idname = 'transform.position_forceps_left_operator'
    
    def execute(self, context):  
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects['Pince_laparo_2']
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        scene = context.scene  
        context.scene.transform_orientation_slots[0].type = 'local_pince_left'
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
        obj.location = scene.custom_props.position_forceps_left_slider
        return {'FINISHED'}       
        
        
############################################
######### RIGHT FORCEPS ACTION  ############
############################################

class TRANSFORM_OT_ORIENTATION_FORCEPS_RIGHT(bpy.types.Operator):
    """ Rotate right forceps """
    
    bl_label = "Change orientation laparoscopic forceps"
    bl_idname = 'transform.orientation_forceps_right_operator'
    
    def execute(self, context):  
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects['Pince_laparo_1']
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        scene = context.scene  
        context.scene.transform_orientation_slots[0].type = 'local_pince_right'
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
        obj.rotation_euler = Euler(scene.custom_props.orientation_forceps_right_slider, 'XYZ')
        return {'FINISHED'}  
        

class TRANSFORM_OT_POSITION_FORCEPS_RIGHT(bpy.types.Operator):
    """ Translate right forceps """
    
    bl_label = "Change position laparoscopic forceps"
    bl_idname = 'transform.position_forceps_right_operator'
    
    def execute(self, context):  
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects['Pince_laparo_1']
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        scene = context.scene  
        context.scene.transform_orientation_slots[0].type = 'local_pince_right'
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='MEDIAN')
        obj.location = scene.custom_props.position_forceps_right_slider
        return {'FINISHED'} 


to_be_registered = [Config_Forceps_Left, Config_Forceps_Right, TRANSFORM_OT_ORIENTATION_FORCEPS_LEFT,
                    TRANSFORM_OT_ORIENTATION_FORCEPS_RIGHT, TRANSFORM_OT_POSITION_FORCEPS_LEFT,
                    TRANSFORM_OT_POSITION_FORCEPS_RIGHT]
