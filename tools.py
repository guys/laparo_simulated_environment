import bpy
from bpy.props import StringProperty
from bpy.props import CollectionProperty


def reg_class(c):
    try:
        bpy.utils.register_class(c)
    except Exception as e:
        print("Exception during register: ", e)
        pass


def unreg_class(c):
    try:
        bpy.utils.unregister_class(c)
    except Exception as e:
        print("Exception during unregister: ", e)
        pass




# define special types for passing parameters
class StringValue(bpy.types.PropertyGroup):
    value: StringProperty(name="Value")


class StringCollection(bpy.types.PropertyGroup):
    value: CollectionProperty(type=StringValue)


def create_custom_property_group(dict_properties):
    class CustomPropertyGroup(bpy.types.PropertyGroup):
        __annotations__ = {}

        def __init__(self):
            pass

    for (k, v) in dict_properties.items():
        CustomPropertyGroup.__annotations__[k] = v
    return CustomPropertyGroup


def register_list_elements(l):
    for i in l:
        print("Register ", i)
        reg_class(i)


def unregister_list_elements(l):
    for i in l:
        print("Unregister ", i)
        unreg_class(i)

